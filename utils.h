#ifndef HW1_UTILS_H
#define HW1_UTILS_H

#include <functional>
#include <numeric>

//template<class T>
//const T& max_value(const std::vector<T>& vec,
//                   std::function<bool(T&, T&)> comparator) {
//    T* max = nullptr;
//    for (auto it = vec.begin(); it != vec.end(); it++) {
//        if (max == nullptr || comparator(&*max, &*it)) {
//            max = &*it;
//        }
//    }
//    return max;
//}
//
//
//void print_graph_statistics(const igraph_wrapper& graph,
//                            const std::string& name) {
//    std::cout << "graph    | " << name << std::endl
//              << "Vertices   : " << graph.vertices_count() << std::endl
//              << "Edges      : " << graph.edges_count() << std::endl
//              //              << "Diameter   : " << graph.diameter() << std::endl // expensive
//              << "Avg. degree: " << graph.average_degree() << std::endl
//              << "<c>        : " << graph.clustering_coefficient() << std::endl;
//}

void print_graph_statistics(const Graph& graph,
                            const std::string& name) {
//    auto components = graph.get_components();
//    std::vector<my_graph::node_t>* lcc = nullptr;
//    for (auto it = components.begin(); it != components.end(); it++) {
//        if (lcc == nullptr || (lcc->size() < it->size())) {
//            lcc = &*it;
//        }
//    }
//    auto lcc_perc = (double) lcc->size() / (double) graph.get_nodes_count() * 100;
    std::cout << "graph    | " << name << std::endl
              << "Nodes    | " << graph.get_nodes_count() << " (" << graph.get_isolated_nodes_count() << ", " << graph.get_pendant_nodes_count() << ")" << std::endl
              << "Edges    | " << graph.get_edges_count() << std::endl
              << "Degree   | " << graph.get_avg_node_degree() << " (" << graph.get_maximum_degree() << ")" << std::endl
              << "Density  | " << graph.get_density() << std::endl
              //              << "Diameter | " << graph.get_diameter() << std::endl
              //              << "   <d>   | " << graph.get_avg_distance() << std::endl
              //              << "   <C>   | " << graph.get_average_clustering() << std::endl
              //              << "LCC     | " << lcc_perc << "% (" << components.size() << ")" << std::endl
              << std::endl;
}

unsigned long long fac(size_t n) {
    unsigned long long result = 1;
    for (size_t i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}

double poisson(double lambda, size_t n) {
    auto p = 1.0;
    for (size_t i = 0; i < n; i++) {
        double x = lambda / ((double) n - (double) i);
        p *= x;
    }
    return p * pow(M_E, -lambda);
}

template <typename T>
std::vector<size_t> sort_with_indices(const std::vector<T> &v, std::function<bool(T, T)> comparator) {
    // https://stackoverflow.com/a/12399290

    // initialize original index locations
    std::vector<size_t> idx(v.size());
    iota(idx.begin(), idx.end(), 0);

    // sort indexes based on comparing values in v
    // using std::stable_sort instead of std::sort
    // to avoid unnecessary index re-orderings
    // when v contains elements of equal values
    std::stable_sort(idx.begin(), idx.end(), [&v, &comparator](auto i1, auto i2) {
        return comparator(v[i1], v[i2]);
    });

    return idx;
}

struct Embellishment {
    std::string name;
    double value;

    Embellishment(const std::string& name, double value) {
        this->name = name;
        this->value = value;
    }
};

double mean(const std::vector<double>& v) {
    double sum = 0.0;
    for (auto it = v.begin(); it != v.end(); it++) {
        sum += *it;
    }
    return sum / (double) v.size();
}

double pearson_correlation_coefficient(const std::vector<double>& x,
                                       const std::vector<double>& y) {
    assert(x.size() == y.size());
    auto mean_x = mean(x);
    auto mean_y = mean(y);
    double numerator = 0.0;
    for (size_t i = 0; i < x.size(); i++) {
        numerator += (x[i] - mean_x) * (y[i] - mean_y);
    }
    double denom_x = 0.0;
    for (size_t i = 0; i < x.size(); i++) {
        denom_x += pow(x[i] - mean_x, 2);
    }
    double denom_y = 0.0;
    for (size_t i = 0; i < x.size(); i++) {
        denom_y += pow(y[i] - mean_y, 2);
    }
    return numerator / (sqrt(denom_x) * sqrt(denom_y));
}

template<class T>
T measure_execution_time(std::function<T(void)> of_task) {
    auto start = std::chrono::high_resolution_clock::now();
    auto result = of_task();
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = duration_cast<std::chrono::seconds>(stop - start);
    std::cout << "Task completed in: " << duration.count() << "[s]" << std::endl;
    return result;
}

#endif //HW1_UTILS_H
