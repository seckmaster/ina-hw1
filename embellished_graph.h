#ifndef HW1_EMBELLISHED_GRAPH_H
#define HW1_EMBELLISHED_GRAPH_H

#include <functional>
#include <unordered_map>
#include <vector>
#include <optional>
#include <Graph.h>

template<class T>
class embellished_graph : public Graph {
    embellished_graph(size_t nodes_count, bool is_directed) : Graph(nodes_count, is_directed) {
    }

public:
    inline T& get_embellishment(Node n) {
        return embellishments.at(n);
    }

    inline const std::vector<T> get_embellishments() const {
        return embellishments;
    }
public:
    static embellished_graph read_edges_list_embellished(const std::string& path,
                                                         size_t nodes_count,
                                                         size_t edges_count,
                                                         bool is_directed,
                                                         std::function<std::optional<T>(const std::string&)> parser) {
        std::ifstream infile(path);
        std::string line;
        std::vector<T> embellishments;
        embellishments.reserve(nodes_count);

        while (true) {
            std::getline(infile, line);
            if (!line.starts_with('#')) { break; }

            auto embellishment = parser(line);
            if (!embellishment.has_value()) { continue; }
            embellishments.push_back(embellishment.value());
            continue;
        }

        embellished_graph<T> graph(nodes_count, is_directed);
        graph.embellishments = embellishments;

        for (size_t i = 0; i < edges_count; i++) {
            if (i > 0) {
                // no need to read the first line as it is already read
                std::getline(infile, line);
            }
            Node from, to;
            sscanf(line.c_str(), "%zu %zu", &from, &to);
            graph.add_edge(from - 1, to - 1);
        }

        return graph;
    }

private:
    std::vector<T> embellishments;
};

#endif //HW1_EMBELLISHED_GRAPH_H
