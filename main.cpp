#include <iostream>
#include <vector>
#include <functional>
#include <unordered_set>
#include <algorithm>
#include <Graph.h>

#include "igraph_wrapper.h"
#include "utils.h"
#include "matplotlibcpp.h"
#include "preferential_random_graph.h"
#include "embellished_graph.h"

int main(int argc, char** argv) {
    {
        // 2.3

        namespace plt = matplotlibcpp;

        // facebook
        auto graph = *Graph::read_edges_list("../data/facebook", 63731, 817035, false);
        auto avg_degree = graph.get_avg_node_degree(); // <k>

        auto degree_distribution = graph.get_degree_distribution();
        auto degree_sequence = std::vector<double>();
        auto probability_sequence = std::vector<double>();
        for (auto it = degree_distribution.begin(); it != degree_distribution.end(); it++) {
            degree_sequence.push_back(it->first);
            probability_sequence.push_back(it->second);
        }

        // erdos-renyi
        auto graph_rnd = Graph::generate_erdos_renyi(graph.get_nodes_count(), graph.get_edges_count(), false);

        auto degree_distribution_rnd = graph_rnd.get_degree_distribution();
        auto degree_sequence_rnd = std::vector<double>();
        auto probability_sequence_rnd = std::vector<double>();
        for (auto it = degree_distribution_rnd.begin(); it != degree_distribution_rnd.end(); it++) {
            degree_sequence_rnd.push_back(it->first);
            probability_sequence_rnd.push_back(it->second);
        }

        // theoretical probability distribution
        auto probability_sequence_rnd_theoretical = std::vector<double>();
        for (auto k : degree_sequence_rnd) {
            double pk = fmax(10e-8, poisson(avg_degree, k));
            probability_sequence_rnd_theoretical.push_back(pk);
        }

        // random preferential graph
        auto graph_pref_rnd_initial_nodes_count = (size_t) ceil(avg_degree) + 1;
        auto graph_pref_rnd = preferential_random_graph::preferential_attachment_model(graph.get_nodes_count(),
                                                                                       graph_pref_rnd_initial_nodes_count,
                                                                                       (size_t) ceil(avg_degree * 0.5));

        auto degree_distribution_pref_rnd = graph_pref_rnd.get_degree_distribution();
        auto degree_sequence_pref_rnd = std::vector<double>();
        auto probability_sequence_pref_rnd = std::vector<double>();
        for (auto it = degree_distribution_pref_rnd.begin(); it != degree_distribution_pref_rnd.end(); it++) {
            degree_sequence_pref_rnd.push_back(it->first);
            probability_sequence_pref_rnd.push_back(it->second);
        }

        plt::named_loglog("facebook", degree_sequence, probability_sequence, ".m");
        plt::named_loglog("erdos-renyi", degree_sequence_rnd, probability_sequence_rnd, ".g");
        plt::named_loglog("theoretical prob. distr.", degree_sequence_rnd, probability_sequence_rnd_theoretical, "b");
        plt::named_loglog("random pref. model", degree_sequence_pref_rnd, probability_sequence_pref_rnd, ".y");
        plt::legend();
        plt::show();
    }

    {
//        {
//            // 1.3
//
//            auto graph = *Graph::read_edges_list("../data/enron", 87273, 1148072, true);
//            print_graph_statistics(graph, "enron");
//
//            std::cout << "Calculating SCCs" << std::endl;
//            auto components = measure_execution_time<std::vector<std::unordered_set<Graph::Node>>>([&graph]() {
//                return graph.get_strong_components();
//            });
//
//            // find LCC
//            std::unordered_set<Graph::Node>* lcc1 = nullptr;
//            for (auto it = components.begin(); it != components.end(); it++) {
//                if (lcc1 == nullptr || (lcc1->size() < it->size())) {
//                    lcc1 = &*it;
//                }
//            }
//            auto lcc_perc = (double) lcc1->size() / (double) graph.get_nodes_count() * 100;
//            std::cout << "LCC      | " << lcc1->size() << " (" << components.size() << ")" << std::endl;
//            std::cout << "<k>      |" << graph.get_avg_node_degree() << std::endl;
//       }
    }
    {
        // 1.5.

        /**
         * Effective diameters (bfs / correct):
         *
         * 2010-2011:
         * 15.325 / 15 (23s)
         *
         * 2010-2012:
         * 12.894 / 13 (280s)
         *
         * 2010-2013:
         * 11.461 // 11 (725s)
         *
         */

//        std::cout << "Effective diameters: " << std::endl;
//
//        auto diameter = measure_execution_time<double>([]() {
//            auto graph = Graph::read_edges_list("../data/aps_2010_2011", 18985, 41574, false).value();
//            std::cout << "Computing d90_1" << std::endl;
//            return graph.get_effective_diameter();
//        });
//        std::cout << diameter << std::endl<<std::endl;
//
//        diameter = measure_execution_time<double>([]() {
//            auto graph = Graph::read_edges_list("../data/aps_2010_2012", 38356, 110066, false).value();
//            std::cout << "Computing d90_2" << std::endl;
//            return graph.get_effective_diameter();
//        });
//        std::cout << diameter << std::endl<<std::endl;
//
//        diameter = measure_execution_time<double>([]() {
//            auto graph = Graph::read_edges_list("../data/aps_2010_2013", 56473, 200592, false).value();
//            std::cout << "Computing d90_3" << std::endl;
//            return graph.get_effective_diameter();
//        });
//        std::cout << diameter << std::endl<<std::endl;

        if (argc > 3) { // NOTE: - this is meant to be run from command line
            auto path = std::string(argv[1]);
            auto nodes = atoi(argv[2]);
            auto edges = atoi(argv[3]);

            std::cout << "Calculating diameter for " << path
                      << ", n: " << nodes << ", m: " << edges << ": " << std::endl;
            auto diameter = measure_execution_time<long>([&path, nodes, edges]() {
                auto graph = Graph::read_edges_list(path, nodes, edges, false).value();
                auto diameter = graph.get_effective_diameter();
                return diameter;
            });
            std::cout << "Diameter: " << diameter << std::endl;
        } else {
            std::cerr << "Invalid args!" << std::endl;
        }
    }

    {
        // 3.
        std::function<std::optional<Embellishment>(const std::string&)> parser = [](const std::string& line) -> std::optional<Embellishment> {
            auto index = line.find('"', line.find(' ') + 1);
            if (index == line.npos) { return std::optional<Embellishment>(); }
            auto next_index = line.find('"', index + 1);
            if (next_index == line.npos) { return std::optional<Embellishment>(); }

            auto name = line.substr(index + 1, next_index - index - 1);

            try {
                auto double_value = std::stod(line.substr(next_index + 2));
                return Embellishment(name, double_value);
            } catch (const std::invalid_argument&) {
                return {};
            } catch (const std::out_of_range&) {
                return {};
            }
        };

        auto highways = embellished_graph<Embellishment>::read_edges_list_embellished("../data/highways", 124, 125, false, parser);

        // clustering coefficient centrality
        auto clustering_coefficients = highways.get_clustering_coefficient_centrality();
        auto clustering_indices = sort_with_indices<double>(clustering_coefficients, [](auto fst, auto snd) {
            return snd < fst;
        });

        for (size_t i = 0; i < 10; i++) {
            std::cout << i + 1 << ". " << highways.get_embellishment(clustering_indices[i]).name << ": "
                      << clustering_coefficients[clustering_indices[i]]
                      << " / "
                      << highways.get_embellishment(clustering_indices[i]).value << std::endl;
        }
        std::cout << std::endl;

        // degree centrality
        auto node_degrees = highways.get_degree_centrality();
        auto degrees_indices = sort_with_indices<double>(node_degrees, [](auto fst, auto snd) { return snd < fst; });

        for (size_t i = 0; i < 10; i++) {
            std::cout << i + 1 << ". " << highways.get_embellishment(degrees_indices[i]).name << ": "
                      << node_degrees[degrees_indices[i]]
                      << " / "
                      << highways.get_embellishment(degrees_indices[i]).value << std::endl;
        }
        std::cout << std::endl;

        // harmonic centrality
        auto harmonic_mean_distances = highways.get_closeness_centrality();
        auto harmonic_means_indices = sort_with_indices<double>(harmonic_mean_distances, [](auto fst, auto snd) {
            return snd < fst;
        });

        for (size_t i = 0; i < 20; i++) {
            std::cout << i + 1 << ". " << highways.get_embellishment(harmonic_means_indices[i]).name << ": "
                      << harmonic_mean_distances[harmonic_means_indices[i]]
                      << " / "
                      << highways.get_embellishment(harmonic_means_indices[i]).value
                      << std::endl;
        }
        std::cout << std::endl;

        // betweeness centrality
        auto highways2 = igraph_wrapper::read_graph_edgelist("../data/highways_igraph", false);
        auto betweeness = highways2.betweeness_non_owned();
        auto betweeness_indices = sort_with_indices<double>(betweeness, [](auto fst, auto snd) {
            return snd < fst;
        });

        auto igraph_harmonic_centrality = highways2.harmonic_mean_distance_non_owned();

        for (size_t i = 0; i < 20; i++) {
            std::cout << i + 1 << ". " << highways.get_embellishment(betweeness_indices[i]).name << ": "
                      << betweeness[betweeness_indices[i]]
                      << " / "
                      << highways.get_embellishment(betweeness_indices[i]).value << std::endl;
        }
        std::cout << std::endl;

        // page rank
        auto page_rank = highways2.page_rank_non_owned();
        auto page_rank2 = highways.get_page_rank_centrality();

        // correlations
        auto embellishments = highways.get_embellishments();
        std::vector<double> traffic_loads;
        traffic_loads.reserve(highways.get_nodes_count());
        std::transform(embellishments.begin(),
                       embellishments.end(),
                       std::back_inserter(traffic_loads),
                       [](auto emb) { return emb.value; });

        auto correlation_cc = pearson_correlation_coefficient(clustering_coefficients, traffic_loads);
        auto correlation_degrees = pearson_correlation_coefficient(node_degrees, traffic_loads);
        auto correlation_harmonic_means = pearson_correlation_coefficient(harmonic_mean_distances, traffic_loads);
        auto correlation_betweeness = pearson_correlation_coefficient(betweeness, traffic_loads);
        auto correlation_page_rank = pearson_correlation_coefficient(page_rank, traffic_loads);

        std::cout << correlation_cc << ", "
                  << correlation_degrees << ", "
                  << correlation_harmonic_means << ", "
                  << correlation_betweeness << ", "
                  << correlation_page_rank << ", "
                  << std::endl;
    }

    return 0;
}
