#ifndef HW1_PREFERENTIAL_RANDOM_GRAPH_H
#define HW1_PREFERENTIAL_RANDOM_GRAPH_H

#include <vector>
#include <unordered_set>
#include <Graph.h>

class preferential_random_graph : public Graph {
public:
    static preferential_random_graph preferential_attachment_model(size_t nodes_count,
                                                                   size_t complete_graph_nodes_count,
                                                                   size_t newly_arrived_edges_count,
                                                                   int seed = 990) {
        auto graph = preferential_random_graph();
        graph.out_adj_list.reserve(nodes_count);
        graph.in_adj_list.reserve(nodes_count);
        graph.is_directed = false;

        graph.build_fully_connected_graph(complete_graph_nodes_count);

        srand(seed);
        for (Node node = complete_graph_nodes_count; node < nodes_count; node++) {
            graph.add_node();
            auto random_nodes = graph.select_random_nodes(node, newly_arrived_edges_count);
            for (auto it = random_nodes.begin(); it != random_nodes.end(); it++) {
                graph.add_edge(node, *it);
            }
        }
        return graph;
    }

    void add_edge(Node from, Node to) override {
        Graph::add_edge(from, to);
        edges.push_back(from);
        edges2.push_back(std::make_pair(from, to));
        if (!is_directed) {
            edges.push_back(to);
            edges2.push_back(std::make_pair(to, from));
        }
    }
private:
    void build_fully_connected_graph(size_t size) {
        for (Node i = 0; i < size; i++) {
            add_node();
            for (Node j = i + 1; j < size; j++) {
                add_edge(i, j);
            }
        }
    }

    std::unordered_set<Node> select_random_nodes(Node first, size_t count) const {
        auto random_nodes = std::unordered_set<Node>();
        random_nodes.reserve(count);
        for (size_t i = 0; i < count; i++) {
            while (true) {
                auto random_node = select_random_node(first);
                if (random_nodes.contains(random_node)) { continue; }
                random_nodes.insert(random_node);
                break;
            }
        }
        return random_nodes;
    }

    Node select_random_node(Node first) const {
        while (true) {
            auto random_node = edges[rand() % edges.size()];
            if (random_node == first) { continue; }
            return random_node;
        }
//        while (true) {
//            auto random_edge = edges2[rand() % edges2.size()];
//            if (random_edge.first == first) {
//                return random_edge.second;
//            }
//            if (random_edge.second == first) {
//                return random_edge.first;
//            }
//            return rand() % 2 == 0 ? random_edge.first : random_edge.second;
//        }
    }
private:
    std::vector<Node> edges; // optimization: only contains starting nodes (from)
    std::vector<std::pair<Node, Node>> edges2;
};

#endif //HW1_PREFERENTIAL_RANDOM_GRAPH_H
